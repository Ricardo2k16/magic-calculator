﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class CProbabilities
    { 

        public static double binomialCoefficients(int n, int k)
        {
            double[] C = new double[k + 1];

            C[0] = 1;

            for (int i = 1; i <= n; i++)
                for (int j = Math.Min(i, k); j > 0; j--)
                    C[j] = C[j] + C[j - 1];

            return C[k];
        }

        public static double hypergeometric(int populationSize, int numberOfSuccessesInPopulation, int sampleSize, int successesInSample)
        {
            double numerador = 1, denominador = 1;

            numerador = binomialCoefficients(numberOfSuccessesInPopulation, successesInSample) *
                binomialCoefficients(populationSize - numberOfSuccessesInPopulation, sampleSize - successesInSample);
            denominador = binomialCoefficients(populationSize, sampleSize);

            return numerador / denominador;
        }

        public static double hypergeometricLT(int populationSize, int numberOfSuccessesInPopulation, int sampleSize, int successesInSample)
        {
            double LT = 0;

            for( int i = successesInSample; i > 0; i--)
            {
                LT += hypergeometric(populationSize, numberOfSuccessesInPopulation, sampleSize, successesInSample-i);
            }

            return LT;
        }
    }
}
