﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MtgApiManager.Lib.Service;

namespace WindowsFormsApp1
{
    public partial class MagicProbabilities : Form
    {
        private SetService setService;
        private CardService cardService;
        private bool hasShown = false;

        public MagicProbabilities()
        {
            InitializeComponent();

            setService = new SetService();
            cardService = new CardService();

            MtgApiManager.Lib.Core.Exceptional<List<MtgApiManager.Lib.Model.Set>> allCards =
                setService.All();

            listBox1.DataSource = allCards.Value;
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Code";
        }

        private void hyperpoliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 calculadora = new Form1();
            calculadora.Show();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( ((ListBox)sender).SelectedItem != null && hasShown )
            {
                Console.WriteLine(((ListBox)sender).SelectedValue.ToString());

                string setCode = ((ListBox)sender).SelectedValue.ToString();
                MtgApiManager.Lib.Core.Exceptional<List<MtgApiManager.Lib.Model.Card>> setCards =
                                                    cardService.Where(x => x.Set, setCode)
                                                    .Where(x => x.Page, 7)
                                                    .Where(x => x.PageSize, 500)
                                                    .All();

                listBox2.DataSource = setCards.Value;
                listBox2.DisplayMember = "Name";
            }
            
        }

        private void MagicProbabilities_Shown(object sender, EventArgs e)
        {
            hasShown = true;
        }
    }
}
