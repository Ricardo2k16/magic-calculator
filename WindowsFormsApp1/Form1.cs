﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            int populationSize = Int32.Parse(txtPopulationSize.Text);
            int numberOfSuccessesInPopulation = Int32.Parse(txtNumberOfSuccessesInPopulation.Text);
            int sampleSize = Int32.Parse(txtSampleSize.Text);
            int successesInSample = Int32.Parse(txtNumberOfSuccessesInSample.Text);

            double hypergeometric = CProbabilities.hypergeometric(populationSize,
                                                                    numberOfSuccessesInPopulation,
                                                                    sampleSize,
                                                                    successesInSample);

            double hypergeometricLT = CProbabilities.hypergeometricLT(populationSize,
                                                                    numberOfSuccessesInPopulation,
                                                                    sampleSize,
                                                                    successesInSample);

            double hypergeometricLTE = (hypergeometric + hypergeometricLT);

            double hypergeometricMT = 1 - hypergeometricLTE;

            double hypergeometricMTE = 1 - hypergeometricLT;

            txtHypergeometricProbability.Text = "" + hypergeometric;
            txtCumulativeProbabilityLT.Text = "" + hypergeometricLT;
            txtCumulativeProbabilityLTE.Text = "" + hypergeometricLTE;
            txtCumulativeProbabilityMT.Text = "" + hypergeometricMT;
            txtCumulativeProbabilityMTE.Text = "" + hypergeometricMTE;
        }
    }
}
