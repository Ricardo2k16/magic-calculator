﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPopulationSize = new System.Windows.Forms.TextBox();
            this.txtNumberOfSuccessesInPopulation = new System.Windows.Forms.TextBox();
            this.txtNumberOfSuccessesInSample = new System.Windows.Forms.TextBox();
            this.txtSampleSize = new System.Windows.Forms.TextBox();
            this.txtCumulativeProbabilityMT = new System.Windows.Forms.TextBox();
            this.txtCumulativeProbabilityLTE = new System.Windows.Forms.TextBox();
            this.txtCumulativeProbabilityLT = new System.Windows.Forms.TextBox();
            this.txtHypergeometricProbability = new System.Windows.Forms.TextBox();
            this.txtCumulativeProbabilityMTE = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Population Size";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of Successes in Population";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sample Size";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Cumulative Probability: P(X < x)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Hypergeometric Probability: P(X = x) \t";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Number of Successes in Sample (x)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(162, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cumulative Probability: P(X >= x) \t";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Cumulative Probability: P(X > x) \t";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(159, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Cumulative Probability: P(X <= x)";
            // 
            // txtPopulationSize
            // 
            this.txtPopulationSize.Location = new System.Drawing.Point(193, 13);
            this.txtPopulationSize.Name = "txtPopulationSize";
            this.txtPopulationSize.Size = new System.Drawing.Size(133, 20);
            this.txtPopulationSize.TabIndex = 9;
            // 
            // txtNumberOfSuccessesInPopulation
            // 
            this.txtNumberOfSuccessesInPopulation.Location = new System.Drawing.Point(193, 46);
            this.txtNumberOfSuccessesInPopulation.Name = "txtNumberOfSuccessesInPopulation";
            this.txtNumberOfSuccessesInPopulation.Size = new System.Drawing.Size(133, 20);
            this.txtNumberOfSuccessesInPopulation.TabIndex = 10;
            // 
            // txtNumberOfSuccessesInSample
            // 
            this.txtNumberOfSuccessesInSample.Location = new System.Drawing.Point(193, 112);
            this.txtNumberOfSuccessesInSample.Name = "txtNumberOfSuccessesInSample";
            this.txtNumberOfSuccessesInSample.Size = new System.Drawing.Size(133, 20);
            this.txtNumberOfSuccessesInSample.TabIndex = 12;
            // 
            // txtSampleSize
            // 
            this.txtSampleSize.Location = new System.Drawing.Point(193, 79);
            this.txtSampleSize.Name = "txtSampleSize";
            this.txtSampleSize.Size = new System.Drawing.Size(133, 20);
            this.txtSampleSize.TabIndex = 11;
            // 
            // txtCumulativeProbabilityMT
            // 
            this.txtCumulativeProbabilityMT.Enabled = false;
            this.txtCumulativeProbabilityMT.Location = new System.Drawing.Point(193, 244);
            this.txtCumulativeProbabilityMT.Name = "txtCumulativeProbabilityMT";
            this.txtCumulativeProbabilityMT.Size = new System.Drawing.Size(133, 20);
            this.txtCumulativeProbabilityMT.TabIndex = 16;
            // 
            // txtCumulativeProbabilityLTE
            // 
            this.txtCumulativeProbabilityLTE.Enabled = false;
            this.txtCumulativeProbabilityLTE.Location = new System.Drawing.Point(193, 211);
            this.txtCumulativeProbabilityLTE.Name = "txtCumulativeProbabilityLTE";
            this.txtCumulativeProbabilityLTE.Size = new System.Drawing.Size(133, 20);
            this.txtCumulativeProbabilityLTE.TabIndex = 15;
            // 
            // txtCumulativeProbabilityLT
            // 
            this.txtCumulativeProbabilityLT.Enabled = false;
            this.txtCumulativeProbabilityLT.Location = new System.Drawing.Point(193, 178);
            this.txtCumulativeProbabilityLT.Name = "txtCumulativeProbabilityLT";
            this.txtCumulativeProbabilityLT.Size = new System.Drawing.Size(133, 20);
            this.txtCumulativeProbabilityLT.TabIndex = 14;
            // 
            // txtHypergeometricProbability
            // 
            this.txtHypergeometricProbability.Enabled = false;
            this.txtHypergeometricProbability.Location = new System.Drawing.Point(193, 145);
            this.txtHypergeometricProbability.Name = "txtHypergeometricProbability";
            this.txtHypergeometricProbability.Size = new System.Drawing.Size(133, 20);
            this.txtHypergeometricProbability.TabIndex = 13;
            // 
            // txtCumulativeProbabilityMTE
            // 
            this.txtCumulativeProbabilityMTE.Enabled = false;
            this.txtCumulativeProbabilityMTE.Location = new System.Drawing.Point(193, 277);
            this.txtCumulativeProbabilityMTE.Name = "txtCumulativeProbabilityMTE";
            this.txtCumulativeProbabilityMTE.Size = new System.Drawing.Size(133, 20);
            this.txtCumulativeProbabilityMTE.TabIndex = 17;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(12, 332);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(314, 40);
            this.btnCalculate.TabIndex = 18;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 380);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.txtCumulativeProbabilityMTE);
            this.Controls.Add(this.txtCumulativeProbabilityMT);
            this.Controls.Add(this.txtCumulativeProbabilityLTE);
            this.Controls.Add(this.txtCumulativeProbabilityLT);
            this.Controls.Add(this.txtHypergeometricProbability);
            this.Controls.Add(this.txtNumberOfSuccessesInSample);
            this.Controls.Add(this.txtSampleSize);
            this.Controls.Add(this.txtNumberOfSuccessesInPopulation);
            this.Controls.Add(this.txtPopulationSize);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Hyper Polinomial Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPopulationSize;
        private System.Windows.Forms.TextBox txtNumberOfSuccessesInPopulation;
        private System.Windows.Forms.TextBox txtNumberOfSuccessesInSample;
        private System.Windows.Forms.TextBox txtSampleSize;
        private System.Windows.Forms.TextBox txtCumulativeProbabilityMT;
        private System.Windows.Forms.TextBox txtCumulativeProbabilityLTE;
        private System.Windows.Forms.TextBox txtCumulativeProbabilityLT;
        private System.Windows.Forms.TextBox txtHypergeometricProbability;
        private System.Windows.Forms.TextBox txtCumulativeProbabilityMTE;
        private System.Windows.Forms.Button btnCalculate;
    }
}

